<?php

session_start();

if (empty($_SESSION['order'])) {
    header("Location: /product_site/product_list.php");
    die();
} else {
    $products = $_SESSION['order'];
}

?>
    <!DOCTYPE HTML>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Study</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
              crossorigin="anonymous">
    </head>

    <body>

    <form action="/product_site/basket.php" method="post">
        <div class="container-xxl text-center col-md-6">
            <h1>Order basket</h1><br>
            <table class="table auto__table text-left" border="5" cellpadding="10">

                <tr class="table-dark">
                    <?php
                    foreach ($products as $product) {
                        foreach ($product as $key => $element) {
                            echo '<th>' . $key . '</th>';
                        }
                        break;
                    }
                    ?>
                </tr>
                <?php
                foreach ($products as $product) {
                    echo '<tr>';
                    foreach ($product as $key => $element) {
                        echo '<td scope="col">' . $element . '</td>';
                    }
                    echo '</tr>';
                }
                ?>
            </table>
            <br>
            <button type="submit" name="product_list">Product list</button>
            <?php
            if (isset($_POST['product_list'])) {
                header("Location: /product_site/product_list.php");
                die();
            }
            ?>
        </div>
    </form>
    </body>
    </html>

