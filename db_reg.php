<?php

require_once "connection.php";

if (isset($_POST['registration'])) {
    $name = $_POST['name'];
    $password = $_POST['password'];
    $email = $_POST['email'];

    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $stmt = $pdo->query("
        SELECT * 
        FROM `users`
        WHERE `user_email` = '$email'
    ");
    $foundMatch = $stmt->fetch();
    if (!$foundMatch) {
        $stmt = $pdo->exec("
        INSERT INTO `users` ( `user_name`, `user_email`, `user_password` )
        VALUES ( '$name', '$email', '$password' );
    ");
        $_SESSION['message'] = 'Hello, ' . $name . '. You successfully registered!';
    } else {
        $_SESSION['message'] = 'User is already registered';
    }
} elseif (isset($_POST['authorization'])) {
    header("Location: /sql_db/db.php");
    die();
}
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>ShopR</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>

<body>
<form action="db_reg.php" method="post">
    <div class="form-group container text-center">
        <label class="form-label" for="name"><b>Name</b></label>
        <input type="text" class="form-control" placeholder="Enter Name" name="name" required>
        <br>
        <!--        <label class="form-label" for="login"><b>Login</b></label>-->
        <!--        <input type="text" class="form-control" placeholder="Enter Login" name="login" required>-->
        <!--        <br>-->
        <label class="form-label" for="password"><b>Password</b></label>
        <input type="text" class="form-control" placeholder="Enter Password" name="password" required>
        <br>
        <label class="form-label" for="email"><b>Email</b></label>
        <input type="text" class="form-control" placeholder="Enter Email" name="email" required>
        <br><br>

        <button type="submit" name="registration" class="btn btn-primary"
        ">Registration</button>
    </div>
</form>

<form action="" method="post">
    <div class="form-group container text-center">
        <br>
        <button type="submit" name="authorization" class="btn btn-primary">Authorization</button>
        <br>
        <?php
        if (isset($_POST['registration'])) {
            echo $_SESSION['message'];
            $_SESSION = [];
        }
        ?>
    </div>
</form>

</body>
</html>