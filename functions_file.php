<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Study</title>
</head>
<body>

<form action="functions_file.php" method="post">
    <div class="container" style="">
        <label for="login"><b>Login</b></label>
        <input type="text" placeholder="Enter Login" name="login" required>
        <br>
        <label for="password"><b>Password</b></label>
        <input type="text" placeholder="Enter Password" name="password" required>
        <br><br>
        <button type="submit" name="registerbtn">Register</button>
    </div>
</form>

</body>
</html>
<?php

function get_array()
{
    return json_decode(file_get_contents('file.json'), true);
}

function get_data($login, $password, $array_users)
{
    foreach ($array_users as $user) {
        if (($user['login'] == $login) and ($user['password'] == $password)) {
            return $user;
        }
        else{
            return false;
        }
    }
}

$login = htmlspecialchars($_POST['login']);
$password = htmlspecialchars($_POST['password']);

$user = get_data($login, $password, get_array());
if($user){
    foreach ($user as $key => $element){
        echo $key.' - '.$element. '<br>';
    }
}
else{
    echo 'User is not found';
}


?>
