<?php

$products = json_decode(file_get_contents("file.json"), true);
session_start();

$array = [];

if (isset($_POST['order'])) {
    foreach ($products as $product) {
        if (!empty($_POST[$product['Name']])) {
            array_push($array, $product);
        }
    }
}
if (isset($_POST['basket'])) {
    header("Location: /product_site/basket.php");
    die();
}

if (count($array) > 0) {
    $_SESSION = [];
    $_SESSION['order'] = $array;
} else {
    $_SESSION['order'] = false;
}

?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Shop</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>

<body>

<form action="/product_site/product_list.php" method="post">
    <div class="container-xxl text-center col-md-6">
        <table class="table auto__table text-left" border="5" cellpadding="10">
            <h1>Products</h1><br>
            <tr class="table-dark">
                <?php
                foreach ($products as $product) {
                    foreach ($product as $key => $element) {
                        echo '<th scope="col">' . $key . '</th>';
                    }
                    break;
                }
                ?>
                <th scope="col">Order</th>
            </tr>
            <?php
            foreach ($products as $product) {
                echo '<tr>';
                foreach ($product as $key => $element) {
                    echo '<td scope="col">' . $element . '</td>';
                }
                echo '<td scope="col"><input type="checkbox" name="' . $product['Name'] . '">Add to order</td>';
                echo '</tr>';
            }
            ?>
        </table>
        <br>
        <button type="submit" name="order">Order goods</button>
        <button type="submit" name="basket">Basket</button>
    </div>
</form>
</body>
</html>



