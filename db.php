<?php

require_once "connection.php";

if (isset($_POST['authorization'])) {
    $login = $_POST['login'];
    $password = $_POST['password'];

    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $stmt = $pdo->prepare("
        SELECT * 
        FROM `users`
        WHERE `user_email` = :login  and `user_password` = :password
    ");
    $stmt->execute(['login' => $login, 'password' => $password]);
    $_SESSION = $stmt->fetch();
}
//    foreach ($result as $value) {
//        if (($value['user_name'] == $login) and ($value['user_password'] == $password)) {
//            echo 'Hello, ' . $login . '!';
//        }
//    }
elseif (isset($_POST['registration'])) {
    header("Location: /sql_db/db_reg.php");
    die();
}
//$result = $pdo->exec("
//    UPDATE users
//    SET `user_password` = 'password1'
//    WHERE
//        `user_id` = (
//            SELECT
//                `u`.`id`
//            FROM
//            ( SELECT MIN( `user_id` ) AS id FROM `users` ) AS u
//        )
//	");
//
//echo '<pre>';
//print_r($result);
//echo '</pre>';

//die();
?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>ShopA</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>

<body>
<form action="db.php" method="post">


    <div class="form-group container text-center">
        <label class="form-label" for="login"><b>Login</b></label>
        <input type="text" class="form-control" placeholder="Enter Login" name="login" required>
        <br>
        <label class="form-label" for="password"><b>Password</b></label>
        <input type="text" class="form-control" placeholder="Enter Password" name="password" required>
        <br>
        <button type="submit" name="authorization" class="btn btn-primary">Authorization</button>

    </div>
</form>
<form action="" method="post">
    <div class="form-group container text-center">
        <br>
        <button type="submit" name="registration" class="btn btn-primary">Registration</button>
        <br>
        <?php
        if (isset($_POST['authorization'])) {
            var_dump($_SESSION);
            echo 'Hello, ' . $_SESSION['user_name'] . ' You successfully logged in!';
            $_SESSION = [];
        }
        ?>
    </div>
</form>

</body>
</html>
