<?php

if (isset($_POST['registration'])) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://api.test.loc/index_registration.php');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);

    $output = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($output, true);


    if (!empty($data['message'])) {
        $result = true;
        $message = $data['message'];
    } else {
        $result = false;
    }
} elseif (isset($_POST['authorization'])) {
    header("Location: /task12/login_form.php");
    die();
}
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>cURL</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>

<body>
<form action="" method="post">
    <div class="form-group container text-center">
        <label class="form-label" for="name"><b>Name</b></label>
        <input type="text" class="form-control" placeholder="Enter Name" name="name" required>
        <br>
        <label class="form-label" for="login"><b>Login</b></label>
        <input type="text" class="form-control" placeholder="Enter Login" name="login" required>
        <br>
        <label class="form-label" for="password"><b>Password</b></label>
        <input type="text" class="form-control" placeholder="Enter Password" name="password" required>
        <br>
        <label class="form-label" for="email"><b>Email</b></label>
        <input type="text" class="form-control" placeholder="Enter Email" name="email" required>
        <br><br>

        <button type="submit" name="registration" class="btn btn-primary"
        ">Registration</button>
    </div>
</form>

<form action="" method="post">
    <div class="form-group container text-center">
        <br>
        <button type="submit" name="authorization" class="btn btn-primary">Authorization</button>
    </div>
</form>

<?php
if ($result) {
    echo $message;
}
?>
</body>
</html>
