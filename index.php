<?php
if(!empty($_POST)){
$users = json_decode(file_get_contents("users.json"),true);


    foreach ($users as $user){
        if (($_POST['login'] == $user['login']) && ($_POST['password'] == $user['password'])){
            $output = ['data' => ['name' => $user['name'], 'id' => $user['id']], 'message' => 'User logged in'];
            echo json_encode($output);
            die();
        }
        elseif (($_POST['login'] == $user['login']) && ($_POST['password'] != $user['password'])){
            $output = ['data' => [], 'message' => 'Wrong password.'];
            echo json_encode($output);
            die();
        }
    }
}
else{
    echo 'Error';
}
?>