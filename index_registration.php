<?php

if (!empty($_POST)) {
    $users = json_decode(file_get_contents("users.json"), true);

    foreach ($users as $user) {
        if ($_POST['login'] == $user['login']) {
            echo json_encode([
                'message' => 'User is already registered.'
            ]);
            die();
        }
    }

    $new_user = [
        'id' => count($users),
        'name' => $_POST['name'],
        'login' => $_POST['login'],
        'password' => $_POST['password'],
        'email' => $_POST['email']
    ];
    array_push($users, $new_user);
    file_put_contents('users.json', json_encode($users));
    echo json_encode([
        'message' => 'Successfully registered new user.'
    ]);
    die();
} else {
    echo 'Error';
}
?>
